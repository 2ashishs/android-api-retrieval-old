package til.ash.api.retrieval.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import til.ash.api.retrieval.R;
import til.ash.api.retrieval.model.PlayLists;

/**
 * Created by ashish on 19/11/15.
 */
public class MyListAdapter extends BaseAdapter {

    Context mContext;
    int mLayout;
    ArrayList<PlayLists.Playlist> mPlaylist;

    LayoutInflater mInflater;

    public MyListAdapter(Context context, int layout, ArrayList<PlayLists.Playlist> playlist) {
        mContext = context;
        mLayout = layout;
        mPlaylist = playlist;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mPlaylist.size();
    }

    @Override
    public Object getItem(int position) {
        if (position > mPlaylist.size()) {
            return null;
        } else {
            return mPlaylist.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(mLayout, null);
            holder = new ViewHolder();
            holder.icon = (ImageView) convertView.findViewById(R.id.list_img);
            holder.title = (TextView) convertView.findViewById(R.id.list_title);
            holder.created_by = (TextView) convertView.findViewById(R.id.list_created_by);
            holder.favorites = (TextView) convertView.findViewById(R.id.list_fav_count);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(mContext).load(mPlaylist.get(position).getArtworkURL()).into(holder.icon);

        holder.title.setText(mPlaylist.get(position).getPlaylistTitle());

        holder.created_by.setText("by "+mPlaylist.get(position).getCreatedBy());

        holder.favorites.setText(mPlaylist.get(position).getFavoriteCount()+" favorites");

        return convertView;
    }

    static class ViewHolder {
        ImageView icon;
        TextView title;
        TextView created_by;
        TextView favorites;
    }
}
