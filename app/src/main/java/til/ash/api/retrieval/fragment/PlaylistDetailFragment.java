package til.ash.api.retrieval.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import til.ash.api.retrieval.Constants;

/**
 * Created by ashish on 25/11/15.
 */
public class PlaylistDetailFragment extends Fragment {

    View parentView = null;
    View containerView;
    String playlistID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (savedInstanceState.getString(Constants.ID_PlayList) != null) {
            playlistID = savedInstanceState.getString(Constants.ID_PlayList);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
