package til.ash.api.retrieval.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ashish on 23/11/15.
 */
public class TrackList implements Serializable {

    @SerializedName("tracks")
    ArrayList<Track> playlistTrack;

    @SerializedName("created_on")
    String createdOn;

    @SerializedName("modified_on")
    String modifiedOn;

    @SerializedName("favorite_count")
    long favCount;

    @SerializedName("count")
    int songCount;

    public class Track implements Serializable {

        @SerializedName("track_id")
        String trackID;

        @SerializedName("track_title")
        String trackTitle;

        @SerializedName("album_title")
        String albumTitle;

        @SerializedName("artwork_web")
        String artwork;

        @SerializedName("artwork_large")
        String artworkLarge;

        @SerializedName("artist")
        ArrayList<Artist> artist;

        public class Artist implements Serializable {

            @SerializedName("name")
            String artistName;
        }
    }
}
