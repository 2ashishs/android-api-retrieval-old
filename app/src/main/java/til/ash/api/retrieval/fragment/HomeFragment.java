package til.ash.api.retrieval.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import til.ash.api.retrieval.Constants;
import til.ash.api.retrieval.MainActivity;
import til.ash.api.retrieval.R;
import til.ash.api.retrieval.adapter.MyListAdapter;
import til.ash.api.retrieval.model.PlayLists;

/**
 * Created by ashish on 20/11/15.
 */
public class HomeFragment extends Fragment {

    View parentView = null;
    View containerView;
    Context mContext;
    LayoutInflater mInflater;

    PlayLists mPlayLists;
    ArrayList<PlayLists.Playlist> mList;
    ListView listView;
    MyListAdapter listAdapter;

    URL url;
    HttpURLConnection connection;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = super.onCreateView(inflater, container, savedInstanceState);
//            containerView = setContentView(R.layout.fragment_home, container);
            containerView = inflater.inflate(R.layout.fragment_home, container);
            mContext = getActivity();
            mInflater = inflater;
        }

        //Check if network is available
        if (isNetworkConnected()) {
            //then make the network call
            new HitAPI().execute(Constants.URL_TopChart);
        } else {
            Toast.makeText(mContext, "Network Problem", Toast.LENGTH_SHORT).show();
        }

        return parentView;
    }

    protected View setContentView(int layoutID, ViewGroup container) {
        return mInflater.inflate(layoutID, container);
    }

    boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return  true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PlayLists.Playlist clickedList = (PlayLists.Playlist) listAdapter.getItem(position);
//                Bundle bundle = new Bundle();
//                bundle.putString(Constants.ID_PlayList, clickedList.getPlaylistId());
//                PlaylistDetailFragment fragment = new PlaylistDetailFragment();
//                fragment.setArguments(bundle);
                mCallback.onPlaylistSelected(clickedList);

            }
        });
    }

    private String inputStreamToJSON(InputStream in) {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        try {
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Reads an InputStream and converts it to a String.
//    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
//        Reader reader = null;
//        reader = new InputStreamReader(stream, "UTF-8");
//        char[] buffer = new char[len];
//        reader.read(buffer);
//        return new String(buffer);
//    }

    private class HitAPI extends AsyncTask<String, Void, PlayLists> {

        @Override
        protected PlayLists doInBackground(String... urls) {
            //Open URL to retrieve data
            try {
                url = new URL(urls[0]);
                connection = (HttpURLConnection) url.openConnection();
                /*Set connection parameters*/
                connection.setReadTimeout(10000 /*ms*/);
                connection.setConnectTimeout(10000 /*ms*/);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);
                /*Connect & receive data*/
                connection.connect();
                int response = connection.getResponseCode();
                Log.d("API", "UrlResponse=" + response);
                /*Read data*/
                InputStream is = connection.getInputStream();
                /*Convert read data to JSON*/
                String readJson = inputStreamToJSON(is);
                Gson gson = new GsonBuilder().create();
                mPlayLists = gson.fromJson(readJson, PlayLists.class);
                return mPlayLists;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(PlayLists mPlayLists) {
            super.onPostExecute(mPlayLists);
            if (mPlayLists != null) {
                mList = mPlayLists.getTopCharts();
                Log.d("API", "Data=" + mList);
                listView = (ListView) containerView.findViewById(R.id.myList);
                listAdapter = new MyListAdapter(mContext, R.layout.item_playlist, mList);
                listView.setAdapter(listAdapter);
            }
        }
    }


    public interface OnPlaylistSelectedListener {
        public void onPlaylistSelected(PlayLists.Playlist mPlaylist);
    }

    OnPlaylistSelectedListener mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            onAttach((MainActivity)context);
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivity) {
            try {
                mCallback = (OnPlaylistSelectedListener)  activity;
            } catch (ClassCastException e) {
//                e.printStackTrace();
                throw new ClassCastException(activity.toString() + " must implement OnPlaylistSelectedListener");
            }
        }
    }
}
