package til.ash.api.retrieval.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ashish on 17/11/15.
 */
public class PlayLists implements Serializable {

    @SerializedName("count")
    int count;

    @SerializedName("user_token_status")
    int user_token_status;

    @SerializedName("playlist")
    ArrayList<Playlist> topCharts;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getUser_token_status() {
        return user_token_status;
    }

    public void setUser_token_status(int user_token_status) {
        this.user_token_status = user_token_status;
    }

    public ArrayList<Playlist> getTopCharts() {
        return topCharts;
    }

    public void setTopCharts(ArrayList<Playlist> topCharts) {
        this.topCharts = topCharts;
    }

    public class Playlist implements Serializable {

        @SerializedName("playlist_id")
        String playlistId;

        @SerializedName("title")
        String playlistTitle;

        @SerializedName("trackids")
        String trackIds;

        @SerializedName("createdby")
        String createdBy;

        @SerializedName("artwork")
        String artworkURL;

        @SerializedName("favorite_count")
        String favoriteCount;

        @SerializedName("language")
        String language;

        public String getPlaylistId() {
            return playlistId;
        }
        public void setPlaylistId(String playlistId) {
            this.playlistId = playlistId;
        }

        public String getPlaylistTitle() {
            return playlistTitle;
        }
        public void setPlaylistTitle(String playlistTitle) {
            this.playlistTitle = playlistTitle;
        }

        public String getTrackIds() {
            return trackIds;
        }
        public void setTrackIds(String trackIds) {
            this.trackIds = trackIds;
        }

        public String getCreatedBy() {
            return createdBy;
        }
        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getArtworkURL() {
            return artworkURL;
        }
        public void setArtworkURL(String artworkURL) {
            this.artworkURL = artworkURL;
        }

        public String getFavoriteCount() {
            return (favoriteCount);
        }
        public void setFavoriteCount(String favoriteCount) {
            this.favoriteCount = favoriteCount;
        }

        public String getLanguage() {
            return language;
        }
        public void setLanguage(String language) {
            this.language = language;
        }
    }
}
