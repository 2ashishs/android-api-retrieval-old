package til.ash.api.retrieval;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import til.ash.api.retrieval.adapter.MyListAdapter;
import til.ash.api.retrieval.fragment.HomeFragment;
import til.ash.api.retrieval.fragment.PlaylistDetailFragment;
import til.ash.api.retrieval.model.PlayLists;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnPlaylistSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container, new HomeFragment()).commit();
    }

    public void displayFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.main_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onPlaylistSelected(PlayLists.Playlist mPlaylist) {
        Bundle args = new Bundle();
        args.putString(Constants.ID_PlayList, mPlaylist.getPlaylistId());
        PlaylistDetailFragment playlistDetailFragment = new PlaylistDetailFragment();
        playlistDetailFragment.setArguments(args);
        displayFragment(playlistDetailFragment);
    }
}
